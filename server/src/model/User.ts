import * as Sequelize from 'sequelize';
export interface UserAttributes{
  id?:number;
  username?:string;
  password?:string;
}
export interface User extends Sequelize.Instance<UserAttributes>{
  id:number;
  username:string;
  password:string;  
}
export interface UserModel extends Sequelize.Model<User,UserAttributes>{
  
}
export default function definedUser(sequelize:Sequelize.Sequelize,DataTypes){
  return sequelize.define('User',{
    username:Sequelize.STRING,
    password:Sequelize.STRING
  });
}