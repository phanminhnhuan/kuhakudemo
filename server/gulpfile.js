var gulp = require("gulp");
var ts = require("gulp-typescript");
var tsProject = ts.createProject("tsconfig.json");
var mocha=require('gulp-mocha');
gulp.task("build", function () {
  return tsProject.src()
      .pipe(tsProject())
      .js.pipe(gulp.dest("dist"));
});
gulp.task("test",function(){
  return gulp.src('test/*.spec.ts')
  .pipe(mocha(
    {
      require:['ts-node/register']
    }));
});
gulp.task("test2",function(){
  return gulp.src('test/*.spec.ts')
  .pipe(ts())
  .pipe(gulp.dest(''))
  .pipe(mocha());
  
});