import 'mocha'
import db from '../src/model/index';
import { UserBusiness } from '../src/logic/UserBusiness';
import { User } from '../src/model/User';
import 'chai'
import { assert } from 'chai'
describe('Test database', function () {
  var users = {};
  before(function (done) {
    console.log('Sync database');
    db.User.sync({ force: true }).then(e => {
      console.log('Drop table if exist');
      db.User.sync({force:true}).then(x => {
        console.log('Create table');
        done()
      }).catch(err=>{
        done(err)
      });
    }).catch(err=>{
      done(err);
    });   
  });
  it('Add User', function (done) {
    db.User.create({ username: 'duckhan', password: '123456' })
      .then(x => {
        done();
      })
      .catch(err => {
        done(err);
      })
  });
  it('Get All User', function (done) {
    var business = new UserBusiness();
    var users = business.getAllUser()
      .then(users => {
        console.log('Total user: ' + users.length);
        done();
      }).catch(err => {
        done(err);
      });
  });
  it('Login with correct user and password', function (done) {
    var business = new UserBusiness();
    var result = business.login({ username: 'duckhan', password: '123456' }, (err, result) => {
      if (err) {
        console.log(err);
      }
      else {
        assert.equal(result, 0);
      }
      done();
    });
  });

  it('Login with user not exist', function (done) {
    var business = new UserBusiness();
    var result = business.login({ username: 'dukhan', password: '123457' }, (err, result) => {
      if (err) {
        done(err);
      }
      else {
        assert.equal(result, -1);
        done();
      }
    });
  });
  
  it('Login with wrong password', function (done) {
    var business = new UserBusiness();
    var result = business.login({ username: 'duckhan', password: '123457' }, (err, result) => {
      if (err) {
        done(err);
      }
      else {
        assert.equal(result, -2);
        done();
      }
    });
  });
});

